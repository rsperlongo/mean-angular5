var mongoose = require('mongoose');

var BookSchema = new mongoose.Schema ({
    isbn: String,
    titulo: String,
    autor: String,
    descricao: String,
    ano_publicacao: String,
    editora: String,
    data_atualizacao: String,
})

module.exports = mongoose.model('Book', BookSchema);